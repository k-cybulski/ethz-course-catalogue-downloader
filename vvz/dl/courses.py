"""
courses.py
==========
These functions are responsible for parsing the list of courses.
"""
from enum import Enum
import unicodedata
import re

from ..codes import DEFAULT_QUERIES_SEARCH, url_with_path
from .cache import fcache_query
from .fetch import get_soup

class RowType(Enum):
    COURSE = 0
    OTHER = 1

RE_NUMBER = re.compile(r'[0-9]+')

def _classify_row(tr):
    # is it one of the header rows?
    if tr.find('th') is not None:
        return RowType.OTHER
    # is it one of the grey rows indicating course category?
    elif tr.find('td', attrs={'class': 'td-level'}):
        return RowType.OTHER
    # it's an ordinary row.
    else:
        column_entries = tr.find_all('td')
        # a course row has 6 entries
        if len(column_entries) == 6:
            return RowType.COURSE
        else:
            return RowType.OTHER

def _wrap_link(url, libreoffice=False):
    if not libreoffice or url is None:
        return url
    else:
        return '=HYPERLINK("{}")'.format(url)

def _parse_row(tr, libreoffice=False):
    row_type = _classify_row(tr)
    out_dict = {
        'row_type': row_type.value # needs to be JSON seriazible
    }
    if row_type is RowType.COURSE:
        tds = tr.find_all('td')
        comment_div = tds[1].find('div', attrs={'class': 'kommentar-le'})
        comment = comment_div.text if comment_div is not None else None
        info_symbol = tds[1].find('a', attrs={'class': 'symbolInfo'})
        info_unwrapped = info_symbol['href'] if info_symbol is not None else None
        info = _wrap_link(info_unwrapped, libreoffice=libreoffice)
        alert_symbol = tds[1].find('a', attrs={'class': 'symbolAlert'})
        alert_unwrapped = url_with_path(alert_symbol['href']) if alert_symbol is not None else None
        alert = _wrap_link(alert_unwrapped, libreoffice=libreoffice)
        out_dict.update(
            {
                'number': tds[0].text,
                'title': tds[1].b.text,
                'link': _wrap_link(url_with_path(tds[1].a['href']),
                                   libreoffice=libreoffice),
                'type': tds[2].a.text,
                'comment': comment,
                'extra_info': info,
                'alert': alert,
                'credits': RE_NUMBER.search(unicodedata.normalize('NFKD',
                                                                  tds[3].text)).group(),
                'lecturers': tds[5].text
            }
        )
    return out_dict

def _parse_courses(soup, debug=False, libreoffice=False):
    trs = soup.find_all('tr')
    rows = [_parse_row(tr, libreoffice=libreoffice) for tr in trs]
    print(rows)
    courses = list(
        filter(
            lambda row: row['row_type'] == RowType.COURSE.value,
            rows
        )
    )
    if debug:
        print("Found {} rows, of which {} are courses".format(len(rows),
                                                              len(courses)))
    return courses

@fcache_query
def _get_courses(query, debug=False, libreoffice=False):
    soup = get_soup(query, debug=debug)
    return _parse_courses(soup, debug=debug, libreoffice=libreoffice)

def get_courses(season_code, programme_code, section_code, debug=False,
                libreoffice=False):
    query = DEFAULT_QUERIES_SEARCH.copy()
    query['semkez'] = season_code
    query['studiengangAbschnittId'] = programme_code
    query['bereichAbschnittId'] = section_code
    return _get_courses(query, debug=debug, libreoffice=libreoffice)
