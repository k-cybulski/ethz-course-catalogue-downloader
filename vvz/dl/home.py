"""
home.py
=======
These functions are responsible for interacting with the homepage, that is
vvz.ethz.ch, to retrieve possible search parameters
"""

from ..codes import (DEFAULT_QUERIES_HOME, DEFAULT_QUERIES_HOME_SEASON,
                     DEFAULT_QUERIES_HOME_PROGRAMME)
from .cache import fcache_query
from .fetch import get_soup

def _get_options_selector(query, id_, debug=False):
    soup = get_soup(query, debug=debug)
    selector = soup.find('select', attrs={'id': id_})
    return [{'code': entry['value'], 'name': entry.text} for entry in selector]

@fcache_query
def _get_semesters(query, debug=False):
    return _get_options_selector(query, 'semkez', debug=debug)

def get_semesters(debug=False):
    return _get_semesters(DEFAULT_QUERIES_HOME, debug=debug)

@fcache_query
def _get_programmes(query, debug=False):
    return _get_options_selector(query, 'studiengangAbschnittId', debug=debug)

def get_programmes(semester_code, debug=False):
    query = DEFAULT_QUERIES_HOME_SEASON.copy()
    query['semkez'] = semester_code
    return _get_programmes(query, debug=debug)

@fcache_query
def _get_sections(query, debug=False):
    return _get_options_selector(query, 'bereichAbschnittId', debug=debug)

def get_sections(semester_code, programme_code, debug=False):
    query = DEFAULT_QUERIES_HOME_PROGRAMME.copy()
    query['semkez'] = semester_code
    query['studiengangAbschnittId'] = programme_code
    return _get_sections(query, debug=debug)
