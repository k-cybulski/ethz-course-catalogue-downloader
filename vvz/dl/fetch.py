import requests
from bs4 import BeautifulSoup
from lxml import etree

from ..codes import URL_ROOT

# no need for us to be smart about sessions, can just use one
SESSION = requests.session()

def get_html(query, debug=False):
    data = SESSION.get(URL_ROOT, params=query).content
    if debug:
        print("Query: {}".format(query))
        print("Data: \n{}".format(data))
    return data

def get_soup(query, debug=False):
    html = get_html(query, debug=debug)
    return BeautifulSoup(html, features="lxml")
