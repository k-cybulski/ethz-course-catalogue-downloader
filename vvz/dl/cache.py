from hashlib import md5
import json
from pathlib import Path
import os

CACHE_DIR = 'cache'

def _make_cachedir():
    path = Path(CACHE_DIR)
    path.mkdir(parents=True, exist_ok=True)

def hash_query(query):
    return md5(json.dumps(query, sort_keys=True).encode('utf-8')).hexdigest()

def present_caches():
    _make_cachedir()
    files = os.listdir(CACHE_DIR)
    return files

def get_cache(hash_):
    _make_cachedir()
    path = os.path.join(CACHE_DIR, hash_)
    with open(path) as file_:
        return json.load(file_)

def put_cache(hash_, data):
    _make_cachedir()
    path = os.path.join(CACHE_DIR, hash_)
    with open(path, 'w') as file_:
        json.dump(data, file_)

def fcache_query(function):
    def _fun(query, **kwargs):
        hash_ = hash_query(query)
        if hash_ in present_caches():
            return get_cache(hash_)
        else:
            data = function(query, **kwargs)
            put_cache(hash_, data)
            return data
    return _fun
