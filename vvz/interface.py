from csv import DictWriter
from datetime import datetime

from .dl.home import get_programmes, get_semesters
from .dl.courses import get_courses

def default_filename():
    now = datetime.now()
    return now.strftime("%Y-%m-%d-%H%M%S.csv")

def make_csv(semester_dicts, semester_programmes, filename=None, debug=False,
             libreoffice=False):
    filename = filename if filename is not None else default_filename()
    with open(filename, 'w') as file_:
        fieldnames = ['number', 'title', 'credits', 'type', 'lecturers',
                      'comment', 'link', 'extra_info', 'alert']
        writer = DictWriter(file_, fieldnames, extrasaction='ignore')
        for semester in semester_dicts:
            writer.writerow({'number': semester['name']})
            for programme in semester_programmes[semester['code']]:
                writer.writerow({'number': programme['name']})
                writer.writeheader()
                courses = get_courses(semester['code'], programme['code'], '',
                                     debug=debug, libreoffice=libreoffice)
                writer.writerows(courses)
                writer.writerow({})
    return filename

def find_semesters_from_input(input_strings, debug=False):
    semesters = get_semesters(debug=debug)
    filtered_semesters = {}
    for semester_dict in semesters:
        for input_string in input_strings:
            if input_string.lower() in semester_dict['name'].lower() or input_string == semester_dict['code']:
                filtered_semesters[semester_dict['code']] = semester_dict['name']
    return [{'code': p[0], 'name': p[1]} for p in filtered_semesters.items()]

def find_programmes_from_input(semester, input_strings, debug=False):
    programmes = get_programmes(semester, debug=debug)
    filtered_programmes = {}
    for programme_dict in programmes:
        for input_string in input_strings:
            if input_string.lower() in programme_dict['name'].lower():
                filtered_programmes[programme_dict['code']] = programme_dict['name']
    return [{'code': p[0], 'name': p[1]} for p in filtered_programmes.items()]

def make_csv_from_input(input_semesters, input_programmes, filename=None,
                        verbose=False, debug=False, libreoffice=False):
    semesters = find_semesters_from_input(input_semesters, debug=debug)
    # only look at one semester, because mostly they repeat and it's a good
    # enough heuristic
    semester_programmes = {
        semester['code']: find_programmes_from_input(semester['code'],
                                                     input_programmes,
                                                     debug=debug)
        for semester in semesters
    }
    if verbose:
        semester_code_to_names = {
            semester['code']: semester['name']
            for semester in semesters
        }
        print("Found programmes:")
        for semester, programmes in semester_programmes.items():
            print("- {} ({})".format(semester_code_to_names[semester], semester))
            for programme in programmes:
                print("  - {}".format(programme['name']))
    return make_csv(semesters, semester_programmes, filename=filename,
                    debug=debug, libreoffice=libreoffice)
