from urllib.parse import urlparse, urlunparse
from os.path import join

URL_ROOT = r"http://vvz.ethz.ch/Vorlesungsverzeichnis/sucheLehrangebot.view"
FIRST_PATH_ELEMENT = 'Vorlesungsverzeichnis'

def url_with_path(path):
    parsed = list(urlparse(URL_ROOT))
    if FIRST_PATH_ELEMENT not in path:
        path = join(FIRST_PATH_ELEMENT, path)
    parsed[2] = path
    return urlunparse(parsed)

# Used to get possible seasons
DEFAULT_QUERIES_HOME = {'lang': ['en']}

# Used to get possible programmes after a season is chosen
DEFAULT_QUERIES_HOME_SEASON = {'lang': 'en',
 'search': 'on',
 'semkez': '2021S', # season
 'studiengangTyp': '',
 'deptId': '',
 'studiengangAbschnittId': '',
 'lerneinheitstitel': '',
 'lerneinheitscode': '',
 'famname': '',
 'rufname': '',
 'wahlinfo': '',
 'lehrsprache': '',
 'periodizitaet': '',
 'katalogdaten': '',
 '_strukturAus': 'on',
 'refresh': 'on'}

# Used to get possible sections after a programme is chosen
DEFAULT_QUERIES_HOME_PROGRAMME = {'lang': 'en',
 'search': 'on',
 'semkez': '', # semester
 'studiengangTyp': '',
 'deptId': '',
 'studiengangAbschnittId': '', # programme ID
 'bereichAbschnittId': '',
 'lerneinheitstitel': '',
 'lerneinheitscode': '',
 'famname': '',
 'rufname': '',
 'wahlinfo': '',
 'lehrsprache': '',
 'periodizitaet': '',
 'katalogdaten': '',
 '_strukturAus': 'on',
 'refresh': 'on'}

# Used to search for courses
DEFAULT_QUERIES_SEARCH = {'lerneinheitscode': [''],
 'deptId': [''],
 'famname': [''],
 'unterbereichAbschnittId': [''],
 'seite': ['0'],
 'lerneinheitstitel': [''],
 'rufname': [''],
 'lehrsprache': [''],
 'bereichAbschnittId': [''],
 'semkez': ['2021W'],
 'studiengangAbschnittId': ['91358'],
 'studiengangTyp': [''],
 'ansicht': ['1'],
 'lang': ['en'],
 'katalogdaten': [''],
 'wahlinfo': ['']}
