# ETHz course info downloader
This is a little Python utility for downloading the list of courses available at ETH
in order to easily put them into spreadsheet software for planning the study plan.

It uses caching to prevent unnecessary downloads in case the same query is performed twice.

## Usage
Install the requirements in your favourite virtual environment
```
pip install -r requirements.txt
```

Then just use the `get_courses.py` script like below
```
python get_courses.py -s "Autumn Semester 2021" -s "Spring Semester 2021" -p "Statistics Master" -p "Mathematics Master" -o output.csv
```

The above line will download all courses from the Statistics and Mathematics Masters from the spring and autumn semesters of 2021, and store them in `output.csv`.
