import argparse

from vvz.dl.home import get_semesters
from vvz.interface import make_csv_from_input

parser = argparse.ArgumentParser(description='Download courses for programmes.')

parser.add_argument('-p', '--programmes', type=str, action='append',
                    help='<Required> programmes to download courses for')
parser.add_argument('-s','--semesters', action='append', help='<Required> semester')
parser.add_argument('-o','--output', help='csv output filename. By default it will be the current timestamp')
parser.add_argument('-d','--debug', action='store_true', help='print debug information')
parser.add_argument('-l','--libreoffice', action='store_true', help='wrap links in "=HYPERLINK("<link>") so libreoffice calc treats them as clickable')
args = parser.parse_args()

if args.semesters is None or args.programmes is None:
    print("""
Please provide at least one semester with the -s option and at least one programme with the -p option.
Available semesters:
""")
    print("{:^10} {:^30}".format("Code", "Name"))
    print("-"*40)
    for semester in get_semesters():
        print("{:^10} {:^30}".format(semester['code'], semester['name']))
else:
    csv_name = make_csv_from_input(args.semesters, args.programmes,
                                   verbose=True, filename=args.output,
                                   debug=args.debug,
                                   libreoffice=args.libreoffice)
    print("Created list of courses: {}".format(csv_name))
